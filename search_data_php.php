<?php
	header('Content-Type: text/html; charset=utf-8');
	$username=$_POST['username'];
	$password=$_POST['password'];
	$database=$_POST['database'];
	$table=$_POST['table'];
	
	$servername = "localhost";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	echo "Connected successfully! <br>";
	
	// Create database
	$sql = "SELECT * FROM $table;";
	echo $sql . "<br>" . "<br>";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		echo "Table List:" . "<br>";
		// output data of each row
		while($row = $result->fetch_assoc()) {
			//echo $row["Tables_in_".$database]. "<br>";
			print_r($row);
			echo "<br>";
		}
	} else {
		echo "0 results";
	}

	$conn->close();
?>