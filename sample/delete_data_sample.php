<html>
<head>
<title>MySQL刪除資料簡例</title>
</head>
<body>
<?php
$servername = "localhost";
$username = $_GET['username'];
$password = $_GET['password'];

$dbname = "test_db";
$table = "basic";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to delete a record
$sql = "DELETE FROM $table WHERE id=1";

if ($conn->query($sql) === TRUE) {
  echo "Record deleted successfully";
} else {
  echo "Error deleting record: " . $conn->error;
}

$conn->close();
?>
</body>
</html>