<html>
<head>
<title>MySQL修改資料簡例</title>
</head>
<body>
<?php
$servername = "localhost";
$username = $_GET['username'];
$password = $_GET['password'];

$dbname = "test_db";
$table = "basic";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE $table SET lastname='Change' WHERE id=1";

if ($conn->query($sql) === TRUE) {
  echo "Record updated successfully";
} else {
  echo "Error updating record: " . $conn->error;
}

$conn->close();
?>
</body>
</html>