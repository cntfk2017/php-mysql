<html>
<head>
<title>MySQL建立資料庫簡例</title>
</head>
<body>
<?php
$servername = "localhost";
$username = $_GET['username'];
$password = $_GET['password'];

$database = "test_db";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE $database";
if ($conn->query($sql) === TRUE) {
  echo "Database $database created successfully";
} else {
  echo "Error creating database: " . $conn->error;
}

$conn->close();
?>
</body>
</html>