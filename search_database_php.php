<?php
	header('Content-Type: text/html; charset=utf-8');
	$username=$_POST['username'];
	$password=$_POST['password'];
	
	$servername = "localhost";

	// Create connection
	$conn = new mysqli($servername, $username, $password);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	echo "Connected successfully! <br>";
	
	// Create database
	$sql = "SHOW DATABASES;";
	echo $sql . "<br>" . "<br>";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		echo "Database List:" . "<br>";
		// output data of each row
		while($row = $result->fetch_assoc()) {
			echo $row["Database"]. "<br>";
		}
	} else {
		echo "0 results";
	}

	$conn->close();
?>