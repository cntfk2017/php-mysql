<?php
	header('Content-Type: text/html; charset=utf-8');
	$username=$_POST['username'];
	$password=$_POST['password'];
	$database=$_POST['database'];
	$table=$_POST['table'];
	$form=$_POST['form'];
	
	$servername = "localhost";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	echo "Connected successfully! <br>";
	
	// sql to create table
	$sql = "CREATE TABLE $table ( $form )";
	echo $sql . "<br>" . "<br>";

	if ($conn->query($sql) === TRUE) {
		echo "Table MyGuests created successfully";
	} else {
		echo "Error creating table: " . $conn->error;
	}

	$conn->close();
?>